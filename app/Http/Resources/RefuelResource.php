<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class RefuelResource
 * @package App\Http\Resources
 */
class RefuelResource extends JsonResource
{
    /**
     * toArray.
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'car_id' => $this->car_id,
            'user_id' => $this->user_id,
            'refuel_quota' => $this->refuel_quota,
            'price_of_litre' => $this->price_of_litre,
            'amount_of_litre' => $this->amount_of_litre,
            'refuel_date' => $this->refuel_date,
            'mileage' => $this->mileage,
        ];
    }
}
