<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class CostResource
 * @package App\Http\Resources
 */
class CostResource extends JsonResource
{
    /**
     * toArray.
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'cost_type_id' => $this->cost_type_id,
            'car_id' => $this->car_id,
            'user_id' => $this->user_id,
            'cost_quota' => $this->cost_quota,
            'cost_date' => $this->cost_date,
            'description' => $this->description,
        ];
    }
}
