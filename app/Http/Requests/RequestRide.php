<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Klasa RequestRide odpowiada za walidację Przejazdu.
 * @package App\Http\Requests
 */
class RequestRide extends FormRequest
{
    /**
     * Funkcja zwraca czy użytkownik jest zalogowany.
     * @return bool
     */
    public function authorize()
    {
        if (Auth::user()) {
            return true;
        } else return false;
    }

    /**
     * Funkcja zwraca warunki do spęłnienia przez Przejazd.
     * @return array
     */
    public function rules()
    {
        $mileage = DB::table('cars')->select('mileage')->where('id', '=', (int)$this->car_id)->get();
        $mileage = json_decode($mileage);
        $mileage = intval($mileage[0]->mileage) - 2000;

        return [
            'user_id' => 'required|numeric',
            'car_id' => 'required|numeric',
            'ride_date' => 'required|date_format:Y-m-d H:i',
            'begin_mileage' => 'required|numeric|min:' . $mileage,
            'end_mileage' => 'required|numeric|min:' . $mileage,
        ];
    }

    /**
     * Funcja zwraca tablicę z błędami walidacji dla Przejazdu.
     * @return array
     */
    public function messages()
    {
        return [
            'user_id.required' => 'Nie wybrano użytkownika',
            'user_id.numeric' => 'Nie wybrano użytkownika',

            'car_id.required' => 'Nie wybrano samochodu',
            'car_id.numeric' => 'Nie wybrano samochodu',

            'ride_date.required' => 'Nie podano daty tankowania',
            'ride_date.date_format' => 'Podano zły format daty i czasu',

            'begin_mileage.required' => 'Nie podano przebiegu',
            'begin_mileage.numeric' => "Przebieg musi być liczbą",
            'begin_mileage.min' => "Stan początkowy nie może być mniejszy niż przebieg",

            'end_mileage.required' => 'Nie podano przebiegu',
            'end_mileage.numeric' => "Przebieg musi być liczbą",
            'end_mileage.min' => "Stan końcowy nie może być mniejszy niż początkowy",
        ];
    }
}
