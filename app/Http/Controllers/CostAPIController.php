<?php

namespace App\Http\Controllers;

use App\Cost;
use App\Http\Requests\RequestCost;
use App\Http\Resources\CostResource;
use Symfony\Component\HttpFoundation\Response;

/**
 * Klasa CostAPIController odpowiada za powiązanie działań aplikacji Android z bazą
 * @package App\Http\Controllers
 */
class CostAPIController extends Controller
{
    /**
     * CostAPIController constructor.
     * Sprawdza czy użytkownik ma uprawnienia do wykonywania funkcji.
     */
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('permission:cost_index', ['only' => ['index', 'show']]);
        $this->middleware('permission:cost_create', ['only' => ['create', 'store']]);
        $this->middleware('permission:cost_edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:cost_delete', ['only' => ['destroy']]);
    }

    /**
     * Funkcja zwraca wszystkie Koszty serwisowe.
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return CostResource::collection(Cost::all());
    }

    /**
     * Funkcja zwraca ostatnie koszty serwisowe Samochodu o indeksie $id.
     * @param int $id indeks samochodu
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function latest($id)
    {
        return CostResource::collection(
            Cost::whereNotNull('created_at')
                ->where('car_id', '=', $id)
                ->orderBy('cost_date', 'desc')
                ->take(5)
                ->get()
        );
    }

    /**
     * Funkcja zapisuje nowy koszt w bazie
     * @param RequestCost $request dane do tabeli
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function store(RequestCost $request)
    {
        $cost = new Cost;
        $cost->cost_type_id = $request->cost_type_id;
        $cost->car_id = $request->car_id;
        $cost->user_id = $request->user_id;
        $cost->cost_quota = $request->cost_quota;
        $cost->cost_date = $request->cost_date;
        $cost->description = ucfirst($request->description);
        $cost->save();

        return response([
            'data' => new CostResource($cost)
        ], Response::HTTP_CREATED);
    }

    /**
     * Funkcja zwraca koszt o podanym $id.
     * @param int $id indeks kosztu
     * @return CostResource
     */
    public function show($id)
    {
        return new CostResource(Cost::find($id));
    }

    /**
     * Funkckja edytuje koszt o podanym $id.
     * @param RequestCost $request
     * @param int $id indeks kosztu
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function update(RequestCost $request, $id)
    {
        $cost = Cost::find($id);
        $cost->cost_type_id = $request->cost_type_id;
        $cost->car_id = $request->car_id;
        $cost->user_id = $request->user_id;
        $cost->cost_quota = $request->cost_quota;
        $cost->cost_date = $request->cost_date;
        $cost->description = ucfirst($request->description);
        $cost->save();

        return response([
            'data' => new CostResource($cost)
        ], Response::HTTP_OK);
    }

    /**
     * Funkcja usuwa z bazy koszt o danym id
     * @param int $id indeks kosztu
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function destroy($id)
    {
        $cost = Cost::find($id);
        $cost->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
