<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


/**
 * Klasa HomeController.
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * HomeController constructor.
     * Sprawdza czy użytkownik ma uprawnienia do wykonywania funkcji.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Funkcja zwraca widok główny aplikacji.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $resultsTankowania = DB::select('SELECT refuels.id as ID, refuels.amount_of_litre  as LITRY, refuel_quota as PLN, brands.name as MARKA, cars.model as MODEL,
cars.registration_number as NRREJ,
users.name as IMIE ,users.surname as NAZWISKO, refuel_date as DAT, users.email as KIEROWCA
  FROM refuels, users, cars, brands
  where refuels.user_id = users.id and cars.id= refuels.car_id and brands.id = cars.brand_id
  order by DAT desc
  limit 0,50;');

        $resultsSerwisowych = DB::select('SELECT costs.id as ID, cost_types.name as KOSZT,  cost_quota as PLN, cost_date as DAT, 
 brands.name as MARKA, cars.model as MODEL, cars.registration_number as NRREJ,
users.name as IMIE , users.surname as NAZWISKO, users.email as KIEROWCA
  FROM costs, users, cost_types, brands, cars
  where costs.user_id = users.id and costs.cost_type_id =cost_types.id and brands.id = cars.brand_id and costs.car_id = cars.id
  order by DAT desc limit 0,50;');

        $roznice = [];
        $dane = DB::table('roznice')->select('roznice.id', 'roznice.ride1_id', 'roznice.ride2_id')
            ->leftJoin('rides', 'roznice.ride1_id', '=', 'rides.id')
            ->orderBy('rides.ride_date')->get()->take(20);
        foreach ($dane as $value) {
            $ride1 = DB::table('rides')->where('id', $value->ride1_id)->first();
            $ride1->user_id = DB::table('users_view')->where('id', $ride1->user_id)->first();
            $ride1->car_id = DB::table('cars_view')->where('id', $ride1->car_id)->first();
            $ride2 = DB::table('rides')->where('id', $value->ride2_id)->first();
            $ride2->user_id = DB::table('users_view')->where('id', $ride2->user_id)->first();
            $ride2->car_id = DB::table('cars_view')->where('id', $ride2->car_id)->first();
            array_push($roznice, ['ride1' => $ride1,
                'ride2' => $ride2]);
        }
//        return $roznice;

        return view('home', ['dane' => $resultsTankowania, 'daneSerwisowe' => $resultsSerwisowych,
            'roznice' => $roznice]);
    }

    /**
     * Funcja zwraca widok do zmiany hasła.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showChangePasswordForm()
    {
        return view('auth.changepassword');
    }

    /**
     * Funkcja zmienia hasło zalogowanego pracownika w bazie.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changePassword(Request $request)
    {

        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error", "Stare hasło jest nieprawidłowe! ");
        } else if (strcmp($request->get('current-password'), $request->get('new-password')) == 0) {
            //Current password and new password are same
            return redirect()->back()->with("error", "Stare hasło i nowe są identyczne! ");
        } else {
            $validatedData = $request->validate([
                'current-password' => 'required',
                'new-password' => 'required | string | min:6 | confirmed',
            ]);

            //Change Password
            $user = Auth::user();
            $user->password = bcrypt($request->get('new-password'));
            $user->save();

            return redirect()->back()->with("success1", "Hasło zmieniono pomyślnie !");
        }
    }
}
