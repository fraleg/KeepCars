<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Car;
use App\Fuel_type;
use App\Http\Requests\RequestCar;
use BaconQrCode\Renderer\Image\Png;
use BaconQrCode\Writer;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

/**
 * Klasa CarController obejmuje funkcje pracujące z Samochodami.
 * @package App\Http\Controllers
 */
class CarController extends Controller
{
    /**
     * CarController constructor.
     * Sprawdza czy użytkownik ma uprawnienia do wywołania funkcji.
     */
    public function __construct()
    {
        $this->middleware('permission:car_index', ['only' => ['index', 'show']]);
        $this->middleware('permission:car_create', ['only' => ['create', 'store']]);
        $this->middleware('permission:car_edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:car_delete', ['only' => ['show', 'destroy']]);
        $this->middleware('permission:car_gen_qr', ['only' => ['qr']]);
    }

    /**
     * Funkcja zwraca widok z listą Samochodów.
     * @return view('car.index')
     */
    public function index()
    {
        $brand_model = Input::has('search_brand_model') ? Input::get('search_brand_model') : null;
        $production_year_from = Input::has('search_production_year_from') ? Input::get('search_production_year_from') : null;
        $production_year_to = Input::has('search_production_year_to') ? Input::get('search_production_year_to') : null;
        $fuel_type = Input::has('search_fuel_type') ? Input::get('search_fuel_type') : null;
        $vin = Input::has('search_vin') ? Input::get('search_vin') : null;
        $registration_number = Input::has('search_registration_number') ? Input::get('search_registration_number') : null;
        $mileage_from = Input::has('search_mileage_from') ? Input::get('search_mileage_from') : null;
        $mileage_to = Input::has('search_mileage_to') ? Input::get('search_mileage_to') : null;

        if (isset($brand_model)) {
            $cars = DB::table('cars')
                ->leftJoin('brands', 'cars.brand_id', '=', 'brands.id')
                ->leftJoin('fuel_types', 'cars.fuel_type_id', '=', 'fuel_types.id')
                ->select('cars.id as id',
                    'brands.name as brand',
                    'cars.model as model',
                    'cars.production_year as production_year',
                    'fuel_types.name as fuel_type',
                    'cars.vin as vin',
                    'cars.registration_number as registration_number',
                    'cars.mileage as mileage'
                )
                ->where('brands.name', 'LIKE', '%' . $brand_model . '%')
                ->orWhere('cars.model', 'LIKE', '%' . $brand_model . '%')
                ->whereNull('cars.deleted_at')
                ->orderBy('cars.brand_id', 'asc')
                ->paginate(10);
        } else if (isset($production_year_from) and isset($production_year_to)) {
            $cars = DB::table('cars')
                ->leftJoin('brands', 'cars.brand_id', '=', 'brands.id')
                ->leftJoin('fuel_types', 'cars.fuel_type_id', '=', 'fuel_types.id')
                ->select('cars.id as id',
                    'brands.name as brand',
                    'cars.model as model',
                    'cars.production_year as production_year',
                    'fuel_types.name as fuel_type',
                    'cars.vin as vin',
                    'cars.registration_number as registration_number',
                    'cars.mileage as mileage'
                )
                ->whereBetween('cars.production_year', [$production_year_from, $production_year_to])
                ->whereNull('cars.deleted_at')
                ->orderBy('cars.brand_id', 'asc')
                ->paginate(10);
        } else if (isset($fuel_type)) {
            $cars = DB::table('cars')
                ->leftJoin('brands', 'cars.brand_id', '=', 'brands.id')
                ->leftJoin('fuel_types', 'cars.fuel_type_id', '=', 'fuel_types.id')
                ->select('cars.id as id',
                    'brands.name as brand',
                    'cars.model as model',
                    'cars.production_year as production_year',
                    'fuel_types.name as fuel_type',
                    'cars.vin as vin',
                    'cars.registration_number as registration_number',
                    'cars.mileage as mileage'
                )
                ->where('fuel_types.name', 'LIKE', '%' . $fuel_type . '%')
                ->whereNull('cars.deleted_at')
                ->orderBy('cars.brand_id', 'asc')
                ->paginate(10);
        } else if (isset($vin)) {
            $cars = DB::table('cars')
                ->leftJoin('brands', 'cars.brand_id', '=', 'brands.id')
                ->leftJoin('fuel_types', 'cars.fuel_type_id', '=', 'fuel_types.id')
                ->select('cars.id as id',
                    'brands.name as brand',
                    'cars.model as model',
                    'cars.production_year as production_year',
                    'fuel_types.name as fuel_type',
                    'cars.vin as vin',
                    'cars.registration_number as registration_number',
                    'cars.mileage as mileage'
                )
                ->where('cars.vin', 'LIKE', '%' . $vin . '%')
                ->whereNull('cars.deleted_at')
                ->orderBy('cars.brand_id', 'asc')
                ->paginate(10);
        } else if (isset($registration_number)) {
            $cars = DB::table('cars')
                ->leftJoin('brands', 'cars.brand_id', '=', 'brands.id')
                ->leftJoin('fuel_types', 'cars.fuel_type_id', '=', 'fuel_types.id')
                ->select('cars.id as id',
                    'brands.name as brand',
                    'cars.model as model',
                    'cars.production_year as production_year',
                    'fuel_types.name as fuel_type',
                    'cars.vin as vin',
                    'cars.registration_number as registration_number',
                    'cars.mileage as mileage'
                )
                ->where('cars.registration_number', 'LIKE', '%' . $registration_number . '%')
                ->whereNull('cars.deleted_at')
                ->orderBy('cars.brand_id', 'asc')
                ->paginate(10);
        } else if (isset($mileage_from) and isset($mileage_to)) {
            $cars = DB::table('cars')
                ->leftJoin('brands', 'cars.brand_id', '=', 'brands.id')
                ->leftJoin('fuel_types', 'cars.fuel_type_id', '=', 'fuel_types.id')
                ->select('cars.id as id',
                    'brands.name as brand',
                    'cars.model as model',
                    'cars.production_year as production_year',
                    'fuel_types.name as fuel_type',
                    'cars.vin as vin',
                    'cars.registration_number as registration_number',
                    'cars.mileage as mileage'
                )
                ->whereBetween('cars.mileage', [$mileage_from, $mileage_to])
                ->whereNull('cars.deleted_at')
                ->orderBy('cars.brand_id', 'asc')
                ->paginate(10);
        } else {
            $cars = DB::table('cars')
                ->leftJoin('brands', 'cars.brand_id', '=', 'brands.id')
                ->leftJoin('fuel_types', 'cars.fuel_type_id', '=', 'fuel_types.id')
                ->select('cars.id as id',
                    'brands.name as brand',
                    'cars.model as model',
                    'cars.production_year as production_year',
                    'fuel_types.name as fuel_type',
                    'cars.vin as vin',
                    'cars.registration_number as registration_number',
                    'cars.mileage as mileage'
                )
                ->whereNull('cars.deleted_at')
                ->orderBy('cars.brand_id', 'asc')
                ->paginate(20);
        }
        return view('car.index')->with('cars', $cars);
    }

    /**
     * Funkcja zwraca widok do dodania samochodu.
     * @return view('car.create')
     */
    public function create()
    {
        $brand = Brand::orderBy('name')->get();
        $fuel_types = Fuel_type::all();

        return view('car.create')->with('allBrands', $brand)->with('allFuelTypes', $fuel_types);
    }

    /**
     * Funkcja dodaje samochód do bazy i zapisuje QR.
     * @param RequestCar $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(RequestCar $request)
    {
        $cars = new Car;
        $cars->brand_id = Input::get('brand_id');
        $cars->model = ucfirst(Input::get('model'));
        $cars->production_year = Input::get('production_year');
        $cars->fuel_type_id = Input::get('fuel_type_id');
        $cars->vin = strtoupper(Input::get('vin'));
        $cars->registration_number = strtoupper(Input::get('registration_number'));
        $cars->mileage = Input::get('mileage');

        if (Input::hasFile('image')) {
            $image = Input::file('image');
            $image->move(public_path() . '/images/cars/', $cars->vin);
        }
        $cars->save();

        //$car = Car::find($id);
        $renderer = new Png();
        $renderer->setHeight(250);
        $renderer->setWidth(250);
        $writer = new Writer($renderer);
        $uniqueCode = $cars->vin;
        $fileName = 'images/QR/' . $cars->vin;
        $writer->writeFile($uniqueCode, $fileName);
        header("Content-Type: image/png");
        echo $writer->writeString($uniqueCode);


        Session::flash('success', 'Dodano nowy samochód z kodem QR pomyślnie!!');

        //$request->session()->flash('success', 'Dodano nowy samochód pomyślnie!');

        return redirect()->route('car.create');
    }

    /**
     * Funkcja zwraca widok do edycji samochodu.
     * @param int $id indeks samochodu
     * @return view('car.edit')
     */
    public function edit($id)
    {
        $cars = Car::find($id);
        $brand = Brand::all();
        $fuel_types = Fuel_type::all();

        return view('car.edit')->with('allBrands', $brand)->with('allCars', $cars)->with('fuel_types', $fuel_types);
    }

    /**
     * Funkcja edytuje samochód z bazy.
     * @param RequestCar $request nowe dane do wprowadzenia
     * @param int $id indeks samochodu
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(RequestCar $request, $id)
    {
        $cars = Car::find($id);
        $cars->brand_id = Input::get('brand_id');
        $cars->model = ucfirst(Input::get('model'));
        $cars->production_year = Input::get('production_year');
        $cars->fuel_type_id = Input::get('fuel_type_id');
        $cars->vin = strtoupper(Input::get('vin'));
        $cars->registration_number = strtoupper(Input::get('registration_number'));
        $cars->mileage = Input::get('mileage');
        $cars->save();
        $request->session()->flash('success', 'Edytowano samochód pomyślnie!');

        return redirect()->route('car.index');
    }

    /**
     * Funkcja usuwa Samochód z bazy.
     * @param int $id indeks samochodu
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $cars = Car::where('id', $id)->first();
        $cars->delete();
        Session::flash('success', 'Pomyślnie usunięto!');

        return redirect()->route('car.index');
    }

    /**
     * Funkcja generuje QR dla danego samochodu.
     * @param int $id indeks Samochodu
     * @return \Illuminate\Http\RedirectResponse
     */
    public function qr($id)
    {
        $car = Car::find($id);
        $renderer = new Png();
        $renderer->setHeight(250);
        $renderer->setWidth(250);
        $writer = new Writer($renderer);
        $uniqueCode = $car->vin;
        $fileName = 'images/QR/' . $car->vin;
        $writer->writeFile($uniqueCode, $fileName);
        header("Content-Type: image/png");
        echo $writer->writeString($uniqueCode);
        Session::flash('success', 'Wygenerowano kod QR pomyślnie!');

        return redirect()->route('car.index');
    }
}
