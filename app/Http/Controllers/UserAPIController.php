<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;

/**
 * Klasa UserAPIController odpowiada za pracę z Pracownikami w aplikacji Android.
 * @package App\Http\Controllers
 */
class UserAPIController extends Controller
{

    /**
     * UserAPIController constructor.
     * Sprawdza czy użytkownik ma uprawnienia do wykonywania funkcji.
     */
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('permission:user_index', ['only' => ['index']]);
        $this->middleware('permission:user_show', ['only' => ['show']]);
        $this->middleware('permission:user_create', ['only' => ['create', 'store']]);
        $this->middleware('permission:user_edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:user_delete', ['only' => ['destroy']]);
    }

    /**
     * Funkcja zwraca dane o wyszstkich Pracownikach.
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return UserResource::collection(User::all());
    }

    /**
     * Funkcja dodaje do bazy nowego Pracownika.
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function store(Request $request)
    {
        $user = new User;
        $user->role_id = $request->role_id;
        $user->name = ucfirst($request->name);
        $user->surname = ucfirst($request->surname);
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();

        return response([
            'data' => new UserResource($user)
        ], Response::HTTP_CREATED);
    }

    /**
     * Funkcja zwraca dane o Pracowniku o podanym id.
     * @param $id
     * @return UserResource
     */
    public function show($id)
    {
        return new UserResource(User::find($id));
    }

    /**
     * Funkcja edytuje w bazie Pracownika o danym id.
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->role_id = $request->role_id;
        $user->name = ucfirst($request->name);
        $user->surname = ucfirst($request->surname);
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();

        return response([
            'data' => new UserResource($user)
        ], Response::HTTP_OK);
    }

    /**
     * Funkcja usuwa z bazy Pracownika o danym id.
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
