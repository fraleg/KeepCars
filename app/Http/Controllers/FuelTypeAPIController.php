<?php

namespace App\Http\Controllers;

use App\Fuel_type;
use App\Http\Requests\RequestFuelType;
use App\Http\Resources\FuelTypeResource;
use Symfony\Component\HttpFoundation\Response;

/**
 * Klasa FuelTypeAPIController odpowiada za pracę z Typami paliw w aplikacji Android.
 * @package App\Http\Controllers
 */
class FuelTypeAPIController extends Controller
{
    /**
     * FuelTypeAPIController constructor.
     * Sprawdza czy użytkownik ma uprawnienia do wykonywania funkcji.
     */
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('permission:fuel_type_index', ['only' => ['index', 'show']]);
        $this->middleware('permission:fuel_type_create', ['only' => ['create', 'store']]);
        $this->middleware('permission:fuel_type_edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:fuel_type_delete', ['only' => ['destroy']]);
    }

    /**
     * Funkcja zwraca dane o wyszstkich typach paliw.
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return FuelTypeResource::collection(Fuel_type::all());
    }

    /**
     * Funkcja dodaje do bazy nowy typ paliwa.
     * @param RequestFuelType $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function store(RequestFuelType $request)
    {
        $fuel_type = new Fuel_type;
        $fuel_type->name = ucfirst($request->name);
        $fuel_type->save();

        return response([
            'data' => new FuelTypeResource($fuel_type)
        ], Response::HTTP_CREATED);
    }

    /**
     * Funkcja zwraca dane o typie paliwa o podanym id.
     * @param $id
     * @return FuelTypeResource
     */
    public function show($id)
    {
        return new FuelTypeResource(Fuel_type::find($id));
    }

    /**
     * Funkcja edytuje w bazie Typ paliwa o danym id
     * @param RequestFuelType $request
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function update(RequestFuelType $request, $id)
    {
        $fuel_type = Fuel_type::find($id);
        $fuel_type->name = ucfirst($request->name);
        $fuel_type->save();

        return response([
            'data' => new FuelTypeResource($fuel_type)
        ], Response::HTTP_OK);
    }

    /**
     * Funkcja usuwa z bazy Typ paliwa o danym id.
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function destroy($id)
    {
        $fuel_type = Fuel_type::find($id);
        $fuel_type->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
