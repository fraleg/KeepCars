<?php
/**
 * Created by PhpStorm.
 * User: Franek
 * Date: 30.03.2018
 * Time: 15:42
 */

namespace App\Exceptions;

use ErrorException;


/**
 * Trait ExceptionTrait
 * @package App\Exceptions
 */
trait ExceptionTrait
{

    /**
     * D.
     * @param $request
     * @param $e
     */
    public function apiException($request, $e)
    {
        if ($this->isError($e)) {
            return $this->ErrorResponse($e);
        }

        return parent::render($request, $e);
    }

    /**
     * isError
     * @param $e
     * @return bool
     */
    protected function isError($e)
    {
        return $e instanceof ErrorException;
    }

    /**
     * ErrorResponse
     * @param $e
     */
    protected function ErrorResponse($e)
    {
//        return response()->json([
//            'errors' => 'Wystapił błąd'
//        ], Response::HTTP_NOT_FOUND);
        //return response()->json('errors.500', 500);
        //abort(500, "fdfdfdf");
    }

}
