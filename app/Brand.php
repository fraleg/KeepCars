<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class Brand odpowiada tabeli brands w bazie.
 *
 * @package App
 */
class Brand extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name',
    ];

    /**
     * Funkcja zwraca Samochody dla danej marki.
     * @return Car::class
     */
    public function cars()
    {
        return $this->hasMany(Car::class);
    }
}
