<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Klasa Car odpowiadające tabeli cars w bazie.
 * @package App
 */
class Car extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'brand_id', 'model', 'production_year', 'fuel_type_id', 'vin', 'registration_number', 'mileage', 'cost_sum', 'mileage_sum', 'litre_sum', 'image_id',
    ];

    /**
     * Zwraca typ paliwa danego samochodu.
     * @return Fuel_type::class
     */
    public function fuel_type()
    {
        return $this->belongsTo(Fuel_type::class);
    }

    /**
     * Zwraca markę samochodu.
     * @return Brand::class
     */
    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    /**
     * Zwraca Tankowania samochodu.
     * @return Refuel::class
     */
    public function refuels()
    {
        return $this->hasMany(Refuel::class);
    }

    /**
     * Zwraca Wydatki serwisowe samochodu.
     * @return Cost::class
     */
    public function costs()
    {
        return $this->hasMany(Cost::class);
    }

    /**
     * Zwraca przejazdy samochodu.
     * @return Ride::class
     */
    public function rides()
    {
        return $this->hasMany(Ride::class);
    }
}
