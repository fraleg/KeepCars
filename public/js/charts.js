function wykresLiniowyPrzebieguKilometrow(canvasID, daty, kilometry) {
    var myLineChart = new Chart(document.getElementById(canvasID), {
        type: 'line',
        data: {
            labels: daty,
            datasets: [{
                label: "Przejechane kilometry",
                lineTension: 0.3,
                backgroundColor: "rgba(2,117,216,0.2)",
                borderColor: "rgba(2,117,216,1)",
                pointRadius: 5,
                pointBackgroundColor: "rgba(2,117,216,1)",
                pointBorderColor: "rgba(255,255,255,0.8)",
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(2,117,216,1)",
                pointHitRadius: 20,
                pointBorderWidth: 2,
                data: kilometry,
            }],
        },
        options: {
            title: {
                display: true,
                text: 'Wykres Liniowy Przebiegu Kilometrów'
            },
            scales: {
                xAxes: [{
                    time: {
                        unit: 'date'
                    },
                    gridLines: {
                        display: false
                    },
                    ticks: {
                        maxTicksLimit: 7
                    }
                }],
                yAxes: [{
                    ticks: {
                        // min: 0,
                        // max: 40000,
                        maxTicksLimit: 5
                    },
                    gridLines: {
                        color: "rgba(0, 0, 0, .125)",
                    }
                }],
            },
            legend: {
                display: false
            }
        }
    });
}



function wykresSlupkowyKosztowPaliwowychOrazSerwisowych(canvasID, daty , koszty1, koszty2 ) {

    var ctx = document.getElementById(canvasID);
    var myLineChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: daty,
            datasets: [{
                label: "Wydatki Paliwowe",
                backgroundColor: "rgba(0, 123, 255,1)",
                borderColor: "rgba(2,117,216,1)",
                data: koszty1,
            },
                {
                    label: "Koszty Serwisowe",
                    backgroundColor: "rgba(220, 53, 69,1)",
                    data: koszty2,
                }],
        },
        options: {
            title: {
                display: true,
                text: 'Wykres kosztów'
            },
            scales: {
                xAxes: [{
                    stacked: true,
                    time: {
                        unit: 'month'
                    },
                    gridLines: {
                        display: false
                    },
                    ticks: {
                        maxTicksLimit: 6
                    }
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        min: 0,
                        // max: 40000,
                        maxTicksLimit: 5
                    },
                    gridLines: {
                        display: true
                    }
                }],
            },
            legend: {
//                position:'right',
                display: true
            }
        }
    });
}


function wykresKolowyPodzialuKosztowNaRodzaje(canvasID, nazwy, kwoty) {

    var ctx = document.getElementById(canvasID);
    var myPieChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: nazwy,
            datasets: [{
                data: kwoty,
                backgroundColor: ['#007bff', '#dc3545', '#ffc107', '#0c1b1f','#aaf442','#008080',
                    '#ff0000', '#00ffff', '#800080', '#660066', '#0099cc', '#fef65b', '#ff4040'],
            }],
        },
        options: {
            title: {
                display: true,
                text: 'Wykres kosztów serwisowych'
            }
        }
    });
}



function wykresDoughnutProporcjiKosztowPaliwowychDoSerwisowych(canvasID,paliwowe, serwisowe) {

    var ctx = document.getElementById(canvasID);
    var myDoughnutChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            labels: ["Koszty Paliwowe", "Koszty Serwisowe"],
            datasets: [{
                data: [paliwowe, serwisowe ],
                backgroundColor: ['#007bff', '#dc3545'],
            }],
        },
    });
}

function lineChartSrednieSpalanie(canvasID, daty, spalania, spalanie2) {

    var myLineChart = new Chart(document.getElementById(canvasID), {
        type: 'line',
        data: {
            labels: daty,
            datasets: [{
                label: "Średnie spalanie Kierowcy",
                lineTension: 0.3,
                backgroundColor: "rgba(2,117,216,0)",
                borderColor: "rgba(2,117,216,1)",
                pointRadius: 5,
                pointBackgroundColor: "rgba(2,117,216,1)",
                pointBorderColor: "rgba(255,255,255,0.8)",
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(2,117,216,1)",
                pointHitRadius: 20,
                pointBorderWidth: 2,
                data: spalania,
            },
                {
                    label: "Średnie spalanie pojazdu",
                    lineTension: 0.3,
                    backgroundColor: "rgba(2,217,216,0)",
                    borderColor: "rgba(2,217,26,1)",
                    pointRadius: 5,
                    pointBackgroundColor: "rgba(2,17,216,1)",
                    pointBorderColor: "rgba(255,255,25,0.8)",
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgba(2,117,216,1)",
                    pointHitRadius: 20,
                    pointBorderWidth: 2,
                    data: spalanie2,
                }],
        },
        options: {
            title: {
                display: true,
                text: 'Wykres Średniego Spalania'
            },
            scales: {
                xAxes: [{
                    time: {
                        unit: 'date'
                    },
                    gridLines: {
                        display: false
                    },
                    ticks: {
                        maxTicksLimit: 7
                    }
                }],
                yAxes: [{
                    ticks: {
                        // min: 0,
                        maxTicksLimit: 5
                    },
                    gridLines: {
                        color: "rgba(0, 0, 0, .125)",
                    }
                }],
            },
            legend: {
                display: true
            }
        }
    });
}


function wykresLiniowyKosztowSerwisowych(canvasID, daty, koszty) {

    var myLineChart = new Chart(document.getElementById(canvasID), {
        type: 'line',
        data: {
            // labels: ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"],
            labels: daty,
            datasets: [{
                label: "Koszty Serwisowe",
                lineTension: 0.1,
                backgroundColor: "rgba(2,117,216,0)",
                borderColor: "rgba(2,117,216,1)",
                pointRadius: 5,
                pointBackgroundColor: "rgba(2,117,216,1)",
                pointBorderColor: "rgba(255,255,255,0.8)",
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(2,117,216,1)",
                pointHitRadius: 20,
                pointBorderWidth: 2,
                data: koszty,
                // data: [0, 120, 260, 1180, 70, 400, 0, 1001, 289, 800, 370, 942],
            }],
        },
        options: {
            title: {
                display: true,
                text: 'Wykres Liniowy Kosztów Serwisowych'
            },
            scales: {
                xAxes: [{
                    time: {
                        unit: 'date'
                    },
                    gridLines: {
                        display: false
                    },
                    ticks: {
                        maxTicksLimit: 7
                    }
                }],
                yAxes: [{
                    ticks: {
                        min: 0,
                        // max: 40000,
                        maxTicksLimit: 5
                    },
                    gridLines: {
                        color: "rgba(0, 0, 0, .125)",
                    }
                }],
            },
            legend: {
                display: false
            }
        }
    });
}