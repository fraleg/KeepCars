@extends('layouts.app')

@section('content')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://files.codepedia.info/files/uploads/iScripts/html2canvas.js"></script>
    <div class="container">
        <div class="card">
            <div class="card-header">
                Generuj raport kierowcy
            </div>
            <div class="card-body">
                <form action="{{route("raport.kierowca.wykres")}}" method="post" onsubmit="move()">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-lg-3 text-center">
                            <h4>Wybierz kierowcę :</h4>
                            <select name="user_id" id="user_id" class="auto-width" required>
                                <option selected="true" disabled="disabled" value="">Wybierz kierowcę...</option>
                                @foreach($users as $key => $value)
                                    <option value="{{$value->id}}"> {{$value->name}} {{$value->surname}}</option>
                                @endforeach
                            </select>
                            <h4 class="mt-2">Wybierz pojazd :</h4>
                            <select id="car_id" name="car_id" class="auto-width">
                                <option value="0">Wszystkie pojazdy</option>
                                @foreach($cars as $key => $value)
                                    <option value="{{$value->id}}"> {{$value->brand->name}} {{$value->model}}
                                        - {{$value->registration_number}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-3 text-center">
                            <h4>Wybierz zakres dat:</h4>
                            <label class="h5 mt-2">Od: &nbsp; </label><input name="data_from" type="text"
                                                                             id="dataOdDoRaportu"
                                                                             required><br>
                            <label class="h5 mt-2">Do: &nbsp;</label><input name="data_to" type="text"
                                                                            id="dataDoDoRaportu"
                                                                            required>

                        </div>
                        <div class="col-lg-3">
                            <h4 class="text-center">Wybierz interwał :</h4>
                            <input type="radio" name="interwalRaport" value="day" required> Dzień<br>
                            <input type="radio" name="interwalRaport" value="week"> Tydzień<br>
                            <input type="radio" name="interwalRaport" value="month"> Miesiąc<br>
                            <input type="radio" name="interwalRaport" value="year">Rok
                        </div>
                        <div class="mt-3 col-lg-3">
                            <button type="submit" class="btn btn-primary">Generuj Raport</button>
                            <a class="btn btn-primary mt-3 text-white" id="btn-Convert-Html2Image">Eksportuj raport do
                                pliku JPG</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div id="statusBar" class="m-auto" style="display: none;">
            <h1 class=" text-center font-weight-bold">Trwa generowanie raportu</h1>
            <div class="loader offset-5"></div>
        </div>
        <script>
            function move() {
                document.getElementById('statusBar').style.display = "block";
            }
        </script>

        <div id="html-content-holder">

            @if(isset($user))


                <div class="card">
                    <div class="card-header bg-dark text-white font-weight-bold font-italic">
                        Raport dla kierowcy {{$user->name}} {{$user->surname}} w okresie : {{$okres}}
                        {{--{{date("d-m-Y", strtotime($data_from))}} - {{date("d-m-Y", strtotime($data_to))}}--}}
                    </div>
                    <div class="card-body">
                        <h2 class="text-muted">{{$user->name}} {{$user->surname}} - {{$user->email}}</h2>
                        <h4 class="text-muted">Stanowisko: {{$user->rola}}</h4>

                    </div>
                </div>
                <div class="card">
                    <div class="card-header bg-dark text-white font-weight-bold font-italic">
                        Statystyki dla wszystkich pojazdów
                    </div>
                    <div class="card">
                        <div class="row">
                            <div class="col-lg-4 my-auto">
                                <table class="index-table w-full">
                                    <tr class="description-tr">
                                        <td>
                                            <div class="font-weight-bold text-primary ">Wydatki ogółem</div>
                                        </td>
                                        <td>
                                            <div class="mb-0 font-weight-bold text-danger">
                                                {{$wydatki_paliwo + $wydatki_serwis}} PLN
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="description-tr">
                                        <td>
                                            <div class="font-weight-bold ">Wydatki paliwowe</div>
                                        </td>
                                        <td>
                                            <div class="mb-0 font-weight-bold text-muted">{{$wydatki_paliwo}} PLN</div>
                                        </td>
                                    </tr>
                                    <tr class="description-tr">
                                        <td>
                                            <div class="font-weight-bold ">Pozostałe wydatki</div>
                                        </td>
                                        <td>
                                            <div class="mb-0 font-weight-bold text-muted">{{$wydatki_serwis}} PLN</div>
                                        </td>
                                    </tr>

                                </table>
                            </div>
                            <div class="col-lg-4 my-auto">
                                <table class="index-table w-full">
                                    <tr class="description-tr">
                                        <td>
                                            <div class="font-weight-bold ">Suma zatankowanych litrów</div>
                                        </td>
                                        <td>
                                            <div class="mb-0 font-weight-bold text-muted">{{$zatankowane_litry}}
                                                litrów
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="description-tr">
                                        <td>
                                            <div class="font-weight-bold ">Ilość tankowań</div>
                                        </td>
                                        <td>
                                            <div class="mb-0 font-weight-bold text-muted">{{$ilosc_tankowan}}</div>
                                        </td>
                                    </tr>
                                    <tr class="description-tr">
                                        <td>
                                            <div class="font-weight-bold ">Ilość innych wydatków</div>
                                        </td>
                                        <td>
                                            <div class="mb-0 font-weight-bold text-muted">{{$ilosc_wydatkow_serwisowych}}</div>
                                        </td>
                                    </tr>


                                </table>
                            </div>
                            <div class="col-lg-4 my-auto">
                                <table class="index-table w-full">
                                    <tr class="description-tr">
                                        <td>
                                            <div class="font-weight-bold  ">Przejechane kilometry</div>
                                        </td>
                                        <td>
                                            <div class="mb-0 font-weight-bold text-muted">{{$przejechane_kilometry}}KM
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="description-tr">
                                        <td>
                                            <div class="font-weight-bold ">Średnie wydatki miesięczne</div>
                                        </td>
                                        <td>
                                            <div class="mb-0 font-weight-bold text-muted">
                                                {{$srednie_wydatki_miesieczne}} PLN
                                            </div>
                                        </td>
                                    </tr>

                                    <tr class="description-tr">
                                        <td>
                                            <div class="font-weight-bold ">Średni przebieg miesięczny</div>
                                        </td>
                                        <td>
                                            <div class="mb-0 font-weight-bold text-muted">{{$sredni_przebieg_miesieczny}}
                                                km
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div>
                            <div class="row">
                                @if(abs($efektywnosc) < 0.5)
                                    <div class="text-right h2 text-info font-weight-bold col-sm-5 align-items-xl-center">
                                        Ekonomiczność: przeciętna
                                    </div>
                                @elseif($efektywnosc >= 0.5)
                                    <div class="text-right h2 text-success font-weight-bold col-sm-5">Ekonomiczność: dobra
                                    </div>
                                @else
                                    <div class="text-right h2 text-danger font-weight-bold col-sm-5">Ekonomiczność: słaba
                                    </div>
                                @endif
                                <div class="h3 text-left text-muted col-sm-7 align-items-xl-center "> Kierowca spala
                                    średnio {{abs($efektywnosc)}} litrów paliwa <?php echo $efektywnosc >= 0 ? "mniej" : "więcej"?>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                Wykresy wygenerowanych kosztów w danym przedziale czasowym
                            </div>
                            <div class="card-body">
                                <canvas id="wszystkiePojazdyUseraWykres4" width="100%" height="30"></canvas>

                                <div class="row">
                                    <div class="col-sm-8 my-auto">
                                        <canvas id="wszystkiePojazdyUseraWykres3" width="100%" height="50"></canvas>
                                    </div>
                                    {{--<div class="col-sm-4 my-auto">--}}
                                    {{--<canvas id="wszystkiePojazdyUseraWykres2" width="100%" height="70"></canvas>--}}
                                    {{--</div>--}}
                                    <div class="col-sm-4 text-center my-auto">
                                        <div class="h4 mb-0 text-primary">{{$srednie_wydatki_paliwowe}} zł</div>
                                        <div class="small text-muted">Średnie wydatki paliwowe</div>
                                        <hr>
                                        <div class="h4 mb-0 text-info">{{$srednie_wydatki_serwisowe}} zł</div>
                                        <div class="small text-muted">Średnie inne wydatki</div>
                                        <hr>
                                        <div class="h4 mb-0 text-danger">{{$max_wydatki_paliwo}} zł</div>
                                        <div class="small text-muted">Największe wydatki paliwowe</div>
                                        <hr>
                                        <div class="h4 mb-0 text-success">{{$min_wydatki_paliwo}} zł</div>
                                        <div class="small text-muted">Najmniejsze wydatki paliwowe</div>

                                    </div>
                                </div>
                                <canvas id="wszystkiePojazdyUseraWykres1" width="100%" height="30"></canvas>
                            </div>
                        </div>
                    </div>

                    @foreach($cars as $key => $value)
                        @if (in_array($value->id, $prowadzone))
                            <div class="card-header bg-dark text-white font-weight-bold font-italic">
                                {{$value->brand->name}} {{$value->model}} {{$value->registration_number}}
                            </div>
                            <div class="card-header">Statystyki kierowcy</div>
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-4 my-auto text-center">
                                            <div class="font-weight-bold text-info mb-4">Wydatki na paliwo</div>
                                            <div class="font-weight-bold">Suma wydatków paliwowych</div>
                                            <div class="h4 mb-0 text-primary">{{$wydatki_paliwo_wszystkie["{$value->id}."]}}
                                                zł
                                            </div>
                                            <hr>
                                            <div class="font-weight-bold">Największe wydatki w okresie</div>
                                            <div class="h4 mb-0 text-danger">{{$max_wydatki_paliwo_wszyscy["{$value->id}."]}}
                                                zł
                                            </div>
                                            <hr>
                                            <div class="font-weight-bold">Najmniejsze wydatki w okresie</div>
                                            <div class="h4 mb-0 text-success"> {{$min_wydatki_paliwo_wszyscy["{$value->id}."]}}
                                                zł
                                            </div>
                                            <hr>
                                            <div class="font-weight-bold">Średnie wydatki w okresie</div>
                                            <div class="h4 mb-0 text-info">{{$avg_wydatki_paliwo_wszyscy["{$value->id}."]}}
                                                zł
                                            </div>
                                        </div>
                                        <div class="col-sm-4 my-auto text-center">
                                            <div class="font-weight-bold text-info mb-4">Przejechane kilometry</div>
                                            <div class="font-weight-bold">Suma kilometrów</div>
                                            <div class="h4 mb-0 text-primary">{{$przejechane_kilometry_wszyscy["{$value->id}."]}}
                                                km
                                            </div>
                                            <hr>
                                            <div class="font-weight-bold">Największa trasa</div>
                                            <div class="h4 mb-0 text-danger">{{$max_kilometry_wszyscy["{$value->id}."]}}
                                                km
                                            </div>
                                            <hr>
                                            <div class="font-weight-bold">Najmniejsza trasa</div>
                                            <div class="h4 mb-0 text-success">{{$min_kilometry_wszyscy["{$value->id}."]}}
                                                km
                                            </div>
                                            <hr>
                                            <div class="font-weight-bold">Średnia długość trasy w okresie</div>
                                            <div class="h4 mb-0 text-info">{{$avg_kilometry_wszyscy["{$value->id}."]}}
                                                km
                                            </div>
                                        </div>
                                        <div class="col-sm-4 my-auto text-center">
                                            <div class="font-weight-bold text-info mb-4">Podsumowanie okresu</div>
                                            <div class="font-weight-bold">Suma pozostałych wydatków w okresie</div>
                                            <div class="h4 mb-0 text-primary">{{$wydatki_serwis_wszyscy["{$value->id}."]}}
                                                zł
                                            </div>
                                            <hr>
                                            <div class="font-weight-bold">Średnie spalanie</div>
                                            <div class="h4 mb-0 text-info   ">{{$avg_spalanie_wszyscy["{$value->id}."]}}
                                                l/100km
                                            </div>
                                            <hr>
                                            <div class="font-weight-bold">Średni przebieg miesięczny</div>
                                            <div class="h4 mb-0 text-info">{{$avg_przebieg_miesiac_wszyscy["{$value->id}."]}}
                                                km
                                            </div>
                                            <hr>
                                            <div class="font-weight-bold">Średnie pozostałe wydatki w okresie</div>
                                            <div class="h4 mb-0 text-info">{{$avg_serwis_wszyscy["{$value->id}."]}}
                                                zł
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <canvas id="{{$value->vin}}_WykresSlukowy" width="100%" height="30"></canvas>

                            <div class="row mt-3 ml-sm-2">
                                <div class="col-lg-3 my-auto">
                                    <canvas id="<?php echo $value->vin; ?>_Kolowy"></canvas>
                                </div>
                                <div class="col-lg-8">
                                    <canvas id="<?php echo $value->vin; ?>_LiniowySpalanie" width="100%"
                                            height="50"></canvas>
                                </div>
                            </div>
                            <div class="col-sm-11 m-auto">
                                <canvas id="<?php echo $value->vin; ?>_LiniowyPrzebiegu" width="100%"
                                        height="30"></canvas>
                            </div>
                        @endif
                    @endforeach
                </div>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
                <script src="{{asset('js/chartsUsers.js')}}"></script>

                <script>

                    var daty = [];
                    var kilometryDoWykresLiniowyPrzebieguKilometrowUSER = [];
                    var koszPaliwoweDoWykresSlupkowyKosztowPaliwowychOrazSerwisowychUSER = [];
                    var koszSerwisoweDoWykresSlupkowyKosztowPaliwowychOrazSerwisowychUSER = [];
                    var pojazdyDoWykresuProporcjiUzywanychPojazdowUSER = [];
                    var przebiegiPojazdowDoWykresuProporcjiUzywanychPojazdowUSER = [];
                    var spalanieKierowcyLineChartSrednieSpalanieUSER = [];

                    kilometryDoWykresLiniowyPrzebieguKilometrowUSER = JSON.parse(<?php echo json_encode($wykres_liniowy_przebieg);?>);
                    daty = JSON.parse(<?php echo json_encode($daty);?>);

                    var diffDays = Math.round(Math.abs((new Date(daty[1]).getTime() - new Date(daty[0]).getTime()) / (86400000)));
                    if (diffDays > 20 && diffDays < 40)
                        for (var i = 0; i < daty.length; i++) {
                            daty[i] = daty[i].substring(0, daty[i].length - 3);
                        }
                    if (diffDays > 300)
                        for (var i = 0; i < daty.length; i++) {
                            daty[i] = daty[i].substring(0, daty[i].length - 6);
                        }
                    if (diffDays > 2 && diffDays < 20) {
                        for (var i = 0; i < daty.length - 1; i++) {
                            daty[i] = daty[i] + "/" + daty[i + 1].substring(8);
                        }
                        // daty[daty.length] = daty[daty.length]+ "/" +String((parseInt(daty[daty.length].substring(8))%31));
                    }


                    koszPaliwoweDoWykresSlupkowyKosztowPaliwowychOrazSerwisowychUSER = JSON.parse(<?php echo json_encode($wykres_slupkowy_koszty_paliwowe);?>);
                    koszSerwisoweDoWykresSlupkowyKosztowPaliwowychOrazSerwisowychUSER = JSON.parse(<?php echo json_encode($wykres_slupkowy_koszty_serwisowe);?>);
                    pojazdyDoWykresuProporcjiUzywanychPojazdowUSER = JSON.parse(<?php echo json_encode($wykres_kolowy_kilometry_pojazdy_cars);?>);
                    przebiegiPojazdowDoWykresuProporcjiUzywanychPojazdowUSER = JSON.parse(<?php echo json_encode($wykres_kolowy_kilometry_pojazdy_kilometry);?>);

                    onload = function () {
                        wykresLiniowyPrzebieguKilometrowUSER('wszystkiePojazdyUseraWykres4', daty, kilometryDoWykresLiniowyPrzebieguKilometrowUSER, koszSerwisoweDoWykresSlupkowyKosztowPaliwowychOrazSerwisowychUSER);
                        wykresDoughnutProporcjiUzywanychPojazdowUSER('wszystkiePojazdyUseraWykres1', pojazdyDoWykresuProporcjiUzywanychPojazdowUSER, przebiegiPojazdowDoWykresuProporcjiUzywanychPojazdowUSER);
                        wykresSlupkowyKosztowPaliwowychOrazSerwisowychUSER('wszystkiePojazdyUseraWykres3', daty, koszPaliwoweDoWykresSlupkowyKosztowPaliwowychOrazSerwisowychUSER, koszSerwisoweDoWykresSlupkowyKosztowPaliwowychOrazSerwisowychUSER);

                        @foreach($cars as $key => $value)
                        @if (in_array($value->id, $prowadzone))
                        //WYKRESY DLA KONKRETNYCH KIEROWÓW DO FOREACH'A

                        var wykresSlupkowyKosztowPaliwowychOrazSerwisowych_paliwo = JSON.parse(<?php echo json_encode(
                            $wykres_slupkowy_koszty_paliwowe_wszyscy["{$value->id}."]);?>);
                        var wykresSlupkowyKosztowPaliwowychOrazSerwisowych_serwis = JSON.parse(<?php echo json_encode(
                            $wykres_slupkowy_koszty_serwisowe_wszyscy["{$value->id}."]);?>);
                        var wykresKolowyProporcjiKm_ja = JSON.parse(<?php echo json_encode(
                            $wykres_kolowy_kilometry_pojazdu_wszyscy["{$value->id}."]['ja']);?>);
                        var wykresKolowyProporcjiKm_inni = JSON.parse(<?php echo json_encode(
                            $wykres_kolowy_kilometry_pojazdu_wszyscy["{$value->id}."]['inni']);?>);
                        spalanieKierowcyLineChartSrednieSpalanieUSER = JSON.parse(<?php echo json_encode(
                            $wykres_liniowy_srednie_spalanie_wszyscy["{$value->id}."]);?>);
                        var kilometryDoWykresLiniowyPrzebieguKilometrowUSER_car = JSON.parse(<?php echo json_encode(
                            $wykres_liniowy_przebieg_wszyscy["{$value->id}."]);?>);
                        var spalanie_car = JSON.parse(<?php echo json_encode($wykres_liniowy_srednie_spalanie_car_wszyscy["{$value->id}."]);?>);


                        wykresSlupkowyKosztowPaliwowychOrazSerwisowychUSER('<?php echo $value->vin; ?>_WykresSlukowy', daty, wykresSlupkowyKosztowPaliwowychOrazSerwisowych_paliwo, wykresSlupkowyKosztowPaliwowychOrazSerwisowych_serwis);
                        lineChartSrednieSpalanieUSER('<?php echo $value->vin; ?>_LiniowySpalanie', daty, spalanieKierowcyLineChartSrednieSpalanieUSER, spalanie_car);
                        wykresLiniowyPrzebieguKilometrowUSER('<?php echo $value->vin; ?>_LiniowyPrzebiegu', daty, kilometryDoWykresLiniowyPrzebieguKilometrowUSER_car);
                        wykresKolowyProporcjiKilometrowTymAutemWStosunkuDoInnychKierowcowUSER('<?php echo $value->vin; ?>_Kolowy', wykresKolowyProporcjiKm_ja, wykresKolowyProporcjiKm_inni);
                        @endif
                        @endforeach

                    }
                </script>
            @endif
        </div>

        <div id="previewImage" style="display: none;"></div>


        <script>
            window.addEventListener("load", function () {
                var element = $("#html-content-holder"); // global variable
                var getCanvas; // global variable
                setTimeout(animation, 3000);

                function animation() {
                    html2canvas(element, {
                        onrendered: function (canvas) {
                            $("#previewImage").append(canvas);
                            getCanvas = canvas;
                        }
                    });
                }

                $("#btn-Convert-Html2Image").on('click', function () {
                    var imgageData = getCanvas.toDataURL("image/png");
                    // Now browser starts downloading it instead of just showing it
                    var newData = imgageData.replace(/^data:image\/png/, "data:application/octet-stream");
                    $("#btn-Convert-Html2Image").attr("download", "raport_kierowca.png").attr("href", newData);
                });
            }, false);


            // $(document).ready(function(){
            // var element = $("#html-content-holder"); // global variable
            // var getCanvas; // global variable
            //
            // $("#btn-Preview-Image").on('click', function () {
            //     html2canvas(element, {
            //         onrendered: function (canvas) {
            //             $("#previewImage").append(canvas);
            //             getCanvas = canvas;
            //         }
            //     });
            // });
            // $("#btn-Convert-Html2Image").on('click', function () {
            //     // html2canvas(element, {
            //     //     onrendered: function (canvas) {
            //     //         $("#previewImage").append(canvas);
            //     //         getCanvas = canvas;
            //     //     }
            //     // });
            //     // var imgageData = getCanvas.toDataURL("image/png");
            //     // // Now browser starts downloading it instead of just showing it
            //     // var newData = imgageData.replace(/^data:image\/png/, "data:application/octet-stream");
            //     // $("#btn-Convert-Html2Image").attr("download", "your_pic_name.png").attr("href", newData);
            // });
            // });

            //          var element = $("#html-content-holder"); // global variable
            //            var getCanvas; // global variable
            //
            //            ladowanieObrazkaDoEksportu();
            //
            //            function ladowanieObrazkaDoEksportu() {
            //                html2canvas(element, {
            //                    function (canvas) {
            //                        document.getElementById('previewImage').append(canvas);
            //                        getCanvas = canvas;
            //                    }
            //                });
            //            }
            //            function drukowanieObrazkaDoEksportu() {
            //                var imgageData = getCanvas.toDataURL("image/png");
            //                // Now browser starts downloading it instead of just showing it
            //                var newData = imgageData.replace(/^data:image\/png/, "data:application/octet-stream");
            //                $("#btn-Convert-Html2Image").attr("download", "raport.png").attr("href", newData);
            //            }
            //

        </script>
    </div>

@endsection
