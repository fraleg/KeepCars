@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dodaj nowy samochód</div>

                    <div class="card-body">

                        <form method="POST" action="{{ route('car.store') }}" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group row">
                                <label for="brand_id" class="col-md-4 col-form-label text-md-right">Marka</label>

                                <div class="col-md-6">
                                    <select id="brand_id" type="number"
                                            class="form-control{{ $errors->has('brand_id') ? ' is-invalid' : '' }}"
                                            name="brand_id">
                                        <option>Wybierz markę...</option>
                                        @foreach($allBrands as $key => $value)
                                            <option value="{{$value->id}}"
                                                    @if(old('brand_id')==$value->id)selected @endif>{{$value->name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('brand_id'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('brand_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="model" class="col-md-4 col-form-label text-md-right">Model</label>

                                <div class="col-md-6">
                                    <input id="model" type="text"
                                           class="form-control{{ $errors->has('model') ? ' is-invalid' : '' }}"
                                           name="model" value="{{ old('model') }}" required>

                                    @if ($errors->has('model'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('model') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="production_year" class="col-md-4 col-form-label text-md-right">Rok
                                    produkcji</label>

                                <div class="col-md-6">
                                    <input id="production_year" type="number"
                                           class="form-control{{ $errors->has('production_year') ? ' is-invalid' : '' }}"
                                           name="production_year" value="{{ old('production_year') }}" required>

                                    @if ($errors->has('production_year'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('production_year') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="fuel_type_id" class="col-md-4 col-form-label text-md-right">Typ
                                    paliwa</label>

                                <div class="col-md-6">
                                    <select id="fuel_type_id" type="text"
                                            class="form-control{{ $errors->has('fuel_type_id') ? ' is-invalid' : '' }}"
                                            name="fuel_type_id">
                                        <option>Wybierz typ paliwa...</option>
                                        @foreach($allFuelTypes as $key => $value)
                                            <option value="{{$value->id}}"
                                                    @if(old('fuel_type_id')==$value->id)selected @endif>{{$value->name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('fuel_type_id'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('fuel_type_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="vin" class="col-md-4 col-form-label text-md-right">VIN</label>

                                <div class="col-md-6">
                                    <input id="vin" type="text"
                                           class="form-control{{ $errors->has('vin') ? ' is-invalid' : '' }}"
                                           name="vin" value="{{ old('vin') }}" pattern=".{17}" required
                                           title="VIN musi mieć 17 znaków">

                                    {{--<div class="alert-warning alert-block text-center" id="vinValidate"style="display: none;"><strong>VIN musi mieć 17 znaków</strong></div><script>function validateForm() {   //walidacja  out--}}
                                    {{--var x = document.getElementById('vin').value;if (x.length != 17) {document.getElementById('vinValidate').style.display = "block";return false;}}function validateForm1() { //of focus--}}
                                    {{--document.getElementById('vinValidate').style.display = "none";}</script>--}}

                                    @if ($errors->has('vin'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('vin') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="registration_number" class="col-md-4 col-form-label text-md-right">Numer
                                    rejestracyjny</label>

                                <div class="col-md-6">
                                    <input id="registration_number" type="text"
                                           class="form-control{{ $errors->has('registration_number') ? ' is-invalid' : '' }}"
                                           name="registration_number" value="{{ old('registration_number') }}"
                                           pattern=".{7}" required title="Numer rejestracyjny musi mieć 7 znaków">

                                    {{--<div class="alert-warning alert-block text-center" id="rejValidate"style="display: none;"><strong>Numer rejestracyjny musi mieć 7 znaków</strong></div><script>function validateForm2() {--}}
                                    {{--var x = document.getElementById('registration_number').value;if (x.length != 7) { //walidacja--}}
                                    {{--document.getElementById('rejValidate').style.display = "block";return false;}}function validateForm3() {document.getElementById('rejValidate').style.display = "none";}</script>--}}

                                    @if ($errors->has('registration_number'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('registration_number') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="mileage" class="col-md-4 col-form-label text-md-right">Przebieg</label>

                                <div class="col-md-6">
                                    <input id="mileage" type="number"
                                           class="form-control{{ $errors->has('mileage') ? ' is-invalid' : '' }}"
                                           name="mileage" value="{{ old('mileage') }}" required>

                                    @if ($errors->has('mileage'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('mileage') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="image" class="col-md-4 col-form-label text-md-right">Zdjęcie</label>

                                <div class="col-md-6">
                                    <label>Wybierz zdjęcie do przesłania:</label>
                                    <input type="file" name="image" id="image">
                                    <input type="hidden" value="{{ csrf_token() }}">
                                </div>
                            </div>

                            <div class="form-group row offset-5 mb-0">
                                <button type="submit" class="btn btn-success">
                                    Dodaj samochód
                                </button>
                        </form>
                        <form action="{{ route('car.index') }}">
                            <button type="submit" class="btn btn-danger mx-sm-2">
                                Powrót
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
