@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dodaj nowe tankowanie</div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('refuel.store') }}">
                            @csrf
                            <div class="form-group row">
                                <label for="car_id" class="col-md-4 col-form-label text-md-right">Samochód</label>
                                <div class="col-md-6">
                                    <select id="car_id" type="number"
                                            class="form-control{{ $errors->has('car_id') ? ' is-invalid' : '' }}"
                                            name="car_id">
                                        <option>Wybierz samochód...</option>
                                        @foreach($allCars as $key => $value)
                                            <option value="{{$value->id}}"
                                                    @if(old('car_id')==$value->id)selected @endif>{{$value->brand->name}} {{$value->model}}
                                                --- {{$value->registration_number}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('car_id'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('car_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="user_id" class="col-md-4 col-form-label text-md-right">Użytkownik</label>

                                <div class="col-md-6">
                                    <select id="user_id" type="number"
                                            class="form-control{{ $errors->has('user_id') ? ' is-invalid' : '' }}"
                                            name="user_id">
                                        <option>Wybierz użytkownika...</option>
                                        @foreach($allUsers as $key => $value)
                                            <option value="{{$value->id}}"
                                                    @if(old('user_id')==$value->id)selected @endif>{{$value->name}} {{$value->surname}}
                                                --- {{$value->email}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('user_id'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('user_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="refuel_quota" class="col-md-4 col-form-label text-md-right">Kwota
                                    tankowania</label>

                                <div class="col-md-6">
                                    <input id="refuel_quota" type="number" step="0.01"
                                           class="form-control{{ $errors->has('refuel_quota') ? ' is-invalid' : '' }}"
                                           name="refuel_quota" value="{{ old('refuel_quota') }}" required
                                           onblur="liczCene()">

                                    @if ($errors->has('refuel_quota'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('refuel_quota') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="price_of_litre" class="col-md-4 col-form-label text-md-right">Cena za
                                    litr</label>

                                <div class="col-md-6">
                                    <input id="price_of_litre" type="number" step="0.01"
                                           class="form-control{{ $errors->has('price_of_litre') ? ' is-invalid' : '' }}"
                                           name="price_of_litre" value="{{ old('price_of_litre') }}" required
                                           onblur="liczCene()">

                                    @if ($errors->has('price_of_litre'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('price_of_litre') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="amount_of_litre" class="col-md-4 col-form-label text-md-right">Ilość
                                    litrów</label>

                                <div class="col-md-6">
                                    <input id="amount_of_litre" type="number" step="0.01"
                                           class="form-control{{ $errors->has('amount_of_litre') ? ' is-invalid' : '' }}"
                                           name="amount_of_litre" value="{{ old('amount_of_litre') }}" required>

                                    @if ($errors->has('amount_of_litre'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('amount_of_litre') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <script>
                                function liczCene() {
                                    document.getElementById('amount_of_litre').value = (
                                        document.getElementById('refuel_quota').value / document.getElementById('price_of_litre').value).toFixed(2);
                                }
                            </script>

                            <div class="form-group row">
                                <label for="refuel_date" class="col-md-4 col-form-label text-md-right">Data
                                    tankowania</label>

                                <div class="col-md-6">
                                    {{--<input id="datetime" type="datetime" class="form-control{{ $errors->has('refuel_date') ? ' is-invalid' : '' }}" name="refuel_date" value="{{ old('refuel_date') }}" required>--}}
                                    <input type="text" name="refuel_date" id="customPicker"
                                           value="{{ old('refuel_date') }}" required
                                           class="form-control{{ $errors->has('refuel_date') ? ' is-invalid' : '' }}"/>

                                    @if ($errors->has('refuel_date'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('refuel_date') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="mileage" class="col-md-4 col-form-label text-md-right">Przebieg</label>

                                <div class="col-md-6">
                                    <input id="mileage" type="number" step="0.1"
                                           class="form-control{{ $errors->has('mileage') ? ' is-invalid' : '' }}"
                                           name="mileage" value="{{ old('mileage') }}" required>

                                    @if ($errors->has('mileage'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('mileage') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row mb-0 offset-5">
                                <button type="submit" class="btn btn-success">
                                    Dodaj tankowanie
                                </button>
                        </form>

                        <form action="{{ route('refuel.index') }}">

                            <button type="submit" class="btn btn-danger mx-sm-2">
                                Powrót
                            </button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
