@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dodaj nowy przejazd</div>

                    <div class="card-body">

                        <form method="POST" action="{{ route('ride.store') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="user_id" class="col-md-4 col-form-label text-md-right">Użytkownik</label>

                                <div class="col-md-6">
                                    <select id="user_id" type="number"
                                            class="form-control{{ $errors->has('user_id') ? ' is-invalid' : '' }}"
                                            name="user_id">
                                        <option>Wybierz użytkownika...</option>
                                        @foreach($users as $key => $value)
                                            <option value="{{$value->id}}"
                                                    @if(old('user_id')==$value->id)selected @endif>{{$value->name}} {{$value->surname}}
                                                --- {{$value->email}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('user_id'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('user_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="car_id" class="col-md-4 col-form-label text-md-right">Samochód</label>

                                <div class="col-md-6">
                                    <select id="car_id" type="number"
                                            class="form-control{{ $errors->has('car_id') ? ' is-invalid' : '' }}"
                                            name="car_id">
                                        <option>Wybierz samochód...</option>
                                        @foreach($cars as $key => $value)
                                            <option value="{{$value->id}}"
                                                    @if(old('car_id')==$value->id)selected @endif>{{$value->brand->name}} {{$value->model}}
                                                --- {{$value->registration_number}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('car_id'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('car_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="ride_date" class="col-md-4 col-form-label text-md-right">Data
                                    przejazdu</label>

                                <div class="col-md-6">
                                    {{--<input id="datetime" type="datetime"--}}
                                    {{--class="form-control{{ $errors->has('ride_date') ? ' is-invalid' : '' }}"--}}
                                    {{--name="ride_date" value="{{ old('ride_date') }}" required>--}}
                                    <input type="text" name="ride_date" id="customPicker" value="{{ old('ride_date') }}"
                                           required
                                           class="form-control{{ $errors->has('ride_date') ? ' is-invalid' : '' }}"/>

                                    @if ($errors->has('ride_date'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('ride_date') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="begin_mileage" class="col-md-4 col-form-label text-md-right">Przebieg
                                    początkowy</label>

                                <div class="col-md-6">
                                    <input id="begin_mileage" type="number"
                                           class="form-control{{ $errors->has('begin_mileage') ? ' is-invalid' : '' }}"
                                           name="begin_mileage" value="{{ old('begin_mileage') }}" required>

                                    @if ($errors->has('begin_mileage'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('begin_mileage') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="end_mileage" class="col-md-4 col-form-label text-md-right">Przebieg
                                    końcowy</label>

                                <div class="col-md-6">
                                    <input id="end_mileage" type="number"
                                           class="form-control{{ $errors->has('end_mileage') ? ' is-invalid' : '' }}"
                                           name="end_mileage" value="{{ old('end_mileage') }}" required>

                                    @if ($errors->has('end_mileage'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('end_mileage') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0 offset-5">

                                <button type="submit" class="btn btn-success">
                                    Dodaj przejazd
                                </button>
                        </form>
                        <form action="{{ route('ride.index') }}">
                            <button type="submit" class="btn btn-danger mx-sm-2">
                                Powrót
                            </button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
