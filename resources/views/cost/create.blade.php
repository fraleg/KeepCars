@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dodaj nowy koszt</div>

                    <div class="card-body">

                        <form method="POST" action="{{ route('cost.store') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="cost_type_id" class="col-md-4 col-form-label text-md-right">Typ
                                    kosztu</label>

                                <div class="col-md-6">
                                    <select id="cost_type_id" type="number"
                                            class="form-control{{ $errors->has('cost_type_id') ? ' is-invalid' : '' }}"
                                            name="cost_type_id">
                                        <option>Wybierz typ kosztu...</option>
                                        @foreach($cost_types as $key => $value)
                                            <option value="{{$value->id}}"
                                                    @if(old('cost_type_id')==$value->id)selected @endif>{{$value->name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('cost_type_id'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('cost_type_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="car_id" class="col-md-4 col-form-label text-md-right">Samochód</label>

                                <div class="col-md-6">
                                    <select id="car_id" type="number"
                                            class="form-control{{ $errors->has('car_id') ? ' is-invalid' : '' }}"
                                            name="car_id">
                                        <option>Wybierz samochód...</option>
                                        @foreach($cars as $key => $value)
                                            <option value="{{$value->id}}"
                                                    @if(old('car_id')==$value->id)selected @endif>{{$value->brand->name}} {{$value->model}}
                                                --- {{$value->registration_number}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('car_id'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('car_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="user_id" class="col-md-4 col-form-label text-md-right">Użytkownik</label>

                                <div class="col-md-6">
                                    <select id="user_id" type="number"
                                            class="form-control{{ $errors->has('user_id') ? ' is-invalid' : '' }}"
                                            name="user_id">
                                        <option>Wybierz użytkownika...</option>
                                        @foreach($users as $key => $value)
                                            <option value="{{$value->id}}"
                                                    @if(old('user_id')==$value->id)selected @endif>{{$value->name}} {{$value->surname}}
                                                --- {{$value->email}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('user_id'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('user_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="cost_quota" class="col-md-4 col-form-label text-md-right">Kwota
                                    kosztu</label>

                                <div class="col-md-6">
                                    <input id="cost_quota" type="number" step="0.01"
                                           class="form-control{{ $errors->has('cost_quota') ? ' is-invalid' : '' }}"
                                           name="cost_quota" value="{{ old('cost_quota') }}" required>

                                    @if ($errors->has('cost_quota'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('cost_quota') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="cost_date" class="col-md-4 col-form-label text-md-right">Data kosztu</label>

                                <div class="col-md-6">
                                    {{--<input id="cost_date" type="datetime" class="form-control{{ $errors->has('cost_date') ? ' is-invalid' : '' }}" name="cost_date" value="{{ old('cost_date') }}" required> --}}
                                    <input type="text" name="cost_date" id="customPicker" value="{{ old('cost_date') }}"
                                           required
                                           class="form-control{{ $errors->has('cost_date') ? ' is-invalid' : '' }}"/>
                                    @if ($errors->has('cost_date'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('cost_date') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="description" class="col-md-4 col-form-label text-md-right">Opis</label>

                                <div class="col-md-6">
                                    <input id="description" type="text"
                                           class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}"
                                           name="description" value="{{ old('description') }}">

                                    @if ($errors->has('description'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row mb-0 offset-5">

                                <button type="submit" class="btn btn-success">
                                    Dodaj koszt
                                </button>

                        </form>

                        <form action="{{ route('cost.index') }}">

                            <button type="submit" class="btn btn-danger mx-sm-2">
                                Powrót
                            </button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
