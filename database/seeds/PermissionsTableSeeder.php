<?php

use App\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        $permissions = [
            // marki samochodów
            // 1
            [
                'name' => 'brand_index',
                'display_name' => 'Przeglądaj marki',
                'description' => 'Pozwala przeglądać marki samochodów'
            ],
            // 2
            [
                'name' => 'brand_create',
                'display_name' => 'Dodaj markę',
                'description' => 'Pozwala dodać nową markę samochodu'
            ],
            // 3
            [
                'name' => 'brand_edit',
                'display_name' => 'Edytuj markę',
                'description' => 'Pozwala edytować markę samochodu'
            ],
            // 4
            [
                'name' => 'brand_delete',
                'display_name' => 'Usuń markę',
                'description' => 'Pozwala usunąć markę samochodu'
            ],

            // samochody
            // 5
            [
                'name' => 'car_index',
                'display_name' => 'Przeglądaj samochody',
                'description' => 'Pozwala przeglądać samochody'
            ],
            // 6
            [
                'name' => 'car_create',
                'display_name' => 'Dodaj samochód',
                'description' => 'Pozwala dodać samochód'
            ],
            // 7
            [
                'name' => 'car_edit',
                'display_name' => 'Edytuj samochód',
                'description' => 'Pozwala edytować samochód'
            ],
            // 8
            [
                'name' => 'car_delete',
                'display_name' => 'Usuń samochód',
                'description' => 'Pozwala usunąć samochód'
            ],
            // 9
            [
                'name' => 'car_gen_qr',
                'display_name' => 'Generuj kod QR dla samochodu',
                'description' => 'Pozwala generować kod Qr dla danego samochodu'
            ],
            // 10
            [
                'name' => 'car_read_qr',
                'display_name' => 'Czytanie kodu QR i uzyskanie id samochodu',
                'description' => 'Pozwala czytać kod QR i uzyskać id samochodu'
            ],

            // koszta
            // 11
            [
                'name' => 'cost_index',
                'display_name' => 'Przeglądaj koszta',
                'description' => 'Pozwala przeglądać koszta'
            ],
            // 12
            [
                'name' => 'cost_create',
                'display_name' => 'Dodaj koszt',
                'description' => 'Pozwala dodać koszt'
            ],
            // 13
            [
                'name' => 'cost_edit',
                'display_name' => 'Edytuj koszt',
                'description' => 'Pozwala edytować koszt'
            ],
            // 14
            [
                'name' => 'cost_delete',
                'display_name' => 'Usuń koszt',
                'description' => 'Pozwala usunąć koszt'
            ],

            // typ kosztu
            // 15
            [
                'name' => 'cost_type_index',
                'display_name' => 'Przeglądaj typy kosztów',
                'description' => 'Pozwala przeglądać typy kosztów'
            ],
            // 16
            [
                'name' => 'cost_type_create',
                'display_name' => 'Dodaj typ kosztu',
                'description' => 'Pozwala dodać typ kosztu'
            ],
            // 17
            [
                'name' => 'cost_type_edit',
                'display_name' => 'Edytuj typ kosztu',
                'description' => 'Pozwala edytować typ kosztu'
            ],
            // 18
            [
                'name' => 'cost_type_delete',
                'display_name' => 'Usuń typ kosztu',
                'description' => 'Pozwala usunąć typ kosztu'
            ],

            // typ paliwa
            // 19
            [
                'name' => 'fuel_type_index',
                'display_name' => 'Przeglądaj typy paliwa',
                'description' => 'Pozwala przeglądać typy paliwa'
            ],
            // 20
            [
                'name' => 'fuel_type_create',
                'display_name' => 'Dodaj typ paliwa',
                'description' => 'Pozwala dodać typ paliwa'
            ],
            // 21
            [
                'name' => 'fuel_type_edit',
                'display_name' => 'Edytuj typ paliwa',
                'description' => 'Pozwala edytować typ paliwa'
            ],
            // 22
            [
                'name' => 'fuel_type_delete',
                'display_name' => 'Usuń typ paliwa',
                'description' => 'Pozwala usunąć typ paliwa'
            ],

            // tankowania
            // 23
            [
                'name' => 'refuel_index',
                'display_name' => 'Przeglądaj tankowania',
                'description' => 'Pozwala przeglądać tankowania'
            ],
            // 24
            [
                'name' => 'refuel_create',
                'display_name' => 'Dodaj tankowanie',
                'description' => 'Pozwala dodać tankowanie'
            ],
            // 25
            [
                'name' => 'refuel_edit',
                'display_name' => 'Edytuj tankowanie',
                'description' => 'Pozwala edytować tankowanie'
            ],
            // 26
            [
                'name' => 'refuel_delete',
                'display_name' => 'Usuń tankowanie',
                'description' => 'Pozwala usunąć tankowanie'
            ],

            // role
            // 27
            [
                'name' => 'role_index',
                'display_name' => 'Przeglądaj role',
                'description' => 'Pozwala przeglądać role'
            ],
            // 28
            [
                'name' => 'role_create',
                'display_name' => 'Dodaj role',
                'description' => 'Pozwala dodać role'
            ],
            // 29
            [
                'name' => 'role_edit',
                'display_name' => 'Edytuj role',
                'description' => 'Pozwala edytować role'
            ],
            // 30
            [
                'name' => 'role_delete',
                'display_name' => 'Usuń role',
                'description' => 'Pozwala usunąć role'
            ],

            // role-user
            // 31
            [
                'name' => 'role_user_index',
                'display_name' => 'Przeglądaj role-user',
                'description' => 'Pozwala przeglądać przypisania rol do userów'
            ],
            // 32
            [
                'name' => 'role_user_create',
                'display_name' => 'Dodaj role-user',
                'description' => 'Pozwala dodać przypisanie roli do usera'
            ],
            // 33
            [
                'name' => 'role_user_edit',
                'display_name' => 'Edytuj role-user',
                'description' => 'Pozwala edytować przypisanie roli do usera'
            ],
            // 34
            [
                'name' => 'role_user_delete',
                'display_name' => 'Usuń role-user',
                'description' => 'Pozwala usunąć przypisanie roli do usera'
            ],

            // permission
            // 35
            [
                'name' => 'permission_index',
                'display_name' => 'Przeglądaj uprawnienia',
                'description' => 'Pozwala przeglądać uprawnienia'
            ],
            // 36
            [
                'name' => 'permission_create',
                'display_name' => 'Dodaj uprawnienie',
                'description' => 'Pozwala dodać uprawnienie'
            ],
            // 37
            [
                'name' => 'permission_edit',
                'display_name' => 'Edytuj uprawnienie',
                'description' => 'Pozwala edytować uprawnienie'
            ],
            // 38
            [
                'name' => 'permission_delete',
                'display_name' => 'Usuń uprawnienie',
                'description' => 'Pozwala usunąć uprawnienie'
            ],

            // permission-role
            // 39
            [
                'name' => 'permission_role_index',
                'display_name' => 'Przeglądaj uprawnienia-rola',
                'description' => 'Pozwala przeglądać uprawnienia-rola'
            ],
            // 40
            [
                'name' => 'permission_role_create',
                'display_name' => 'Dodaj uprawnienie-rola',
                'description' => 'Pozwala dodać uprawnienie-rola'
            ],
            // 41
            [
                'name' => 'permission_role_edit',
                'display_name' => 'Edytuj uprawnienie-rola',
                'description' => 'Pozwala edytować uprawnienie-rola'
            ],
            // 42
            [
                'name' => 'permission_role_delete',
                'display_name' => 'Usuń uprawnienie-rola',
                'description' => 'Pozwala usunąć uprawnienie-rola'
            ],

            // ride
            // 43
            [
                'name' => 'ride_index',
                'display_name' => 'Przeglądaj przejazdy',
                'description' => 'Pozwala przeglądać przejazdy'
            ],
            // 44
            [
                'name' => 'ride_create',
                'display_name' => 'Dodaj przejazd',
                'description' => 'Pozwala dodać przejazd'
            ],
            // 45
            [
                'name' => 'ride_edit',
                'display_name' => 'Edytuj przejazd',
                'description' => 'Pozwala edytować przejazd'
            ],
            // 46
            [
                'name' => 'ride_delete',
                'display_name' => 'Usuń przejazd',
                'description' => 'Pozwala usunąć przejazd'
            ],

            // użytkownicy
            // 47
            [
                'name' => 'user_index',
                'display_name' => 'Przeglądaj użytkowników',
                'description' => 'Pozwala przeglądać użytkowników'
            ],
            // 48
            [
                'name' => 'user_create',
                'display_name' => 'Dodaj użytkownika',
                'description' => 'Pozwala dodać użytkownika'
            ],
            // 49
            [
                'name' => 'user_edit',
                'display_name' => 'Edytuj użytkownika',
                'description' => 'Pozwala edytować użytkownika'
            ],
            // 50
            [
                'name' => 'user_delete',
                'display_name' => 'Usuń użytkownika',
                'description' => 'Pozwala usunąć użytkownika'
            ],
            // 51
            [
                'name' => 'user_show',
                'display_name' => 'Pokaż użytkownika',
                'description' => 'Pozwala pokazać użytkownika'
            ],
            // 52
            [
                'name' => 'refuel_latest',
                'display_name' => 'Pokaż ostatnie tankowania',
                'description' => 'Pozwala pokazać ostatnie 10 tankowań samochodu'
            ],
            // 53
            [
                'name' => 'cost_latest',
                'display_name' => 'Pokaż ostatnie koszty',
                'description' => 'Pozwala pokazać ostatnie 5 kosztów samochodu'
            ],
        ];

        foreach ($permissions as $key => $value) {
            Permission::create($value);
        }
    }
}