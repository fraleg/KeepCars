<?php

use App\CostType;
use Illuminate\Database\Seeder;

class CostTypesTableSeeder extends Seeder
{
    public function run()
    {
        $cost_types = [

            [
                'name' => 'Serwis'
            ],
            [
                'name' => 'Eksploatacja'
            ],
            [
                'name' => 'Rejestracja'
            ],
            [
                'name' => 'Parking'
            ],
            [
                'name' => 'Myjnia'
            ],
            [
                'name' => 'Opłata za przejazd'
            ],
            [
                'name' => 'Mandaty'
            ],
            [
                'name' => 'Tuning'
            ],
            [
                'name' => 'Ubezpieczenie'
            ],
        ];

        foreach ($cost_types as $key => $value) {
            CostType::create($value);
        }
    }
}
