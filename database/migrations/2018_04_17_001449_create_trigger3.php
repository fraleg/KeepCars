<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateTrigger3 extends Migration
{
    public function up()
    {
        DB::unprepared('
        CREATE DEFINER=`root`@`localhost` TRIGGER `keepcars`.`rides_BEFORE_INSERT` BEFORE INSERT ON `rides` FOR EACH ROW
            BEGIN
	            UPDATE cars 
                SET mileage = new.end_mileage
                WHERE id = new.car_id;
            END
        ');
    }

    public function down()
    {
        DB::unprepared('DROP TRIGGER `keepcars`.`rides_BEFORE_INSERT`');
    }
}
